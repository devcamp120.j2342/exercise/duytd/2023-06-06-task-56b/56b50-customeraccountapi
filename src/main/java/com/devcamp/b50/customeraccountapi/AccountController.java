package com.devcamp.b50.customeraccountapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b50.customeraccountapi.models.Account;
import com.devcamp.b50.customeraccountapi.models.Customer;

@RestController
@RequestMapping("/api")
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getAccounts(){
        Customer customer1 = new Customer(1, "Maaja", 10);
        Customer customer2 = new Customer(2, "Đoàn Ngọc Minh", 20);
        Customer customer3 = new Customer(3, "Eran katz", 30);

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Account account1 = new Account(1, customer1, 10);
        Account account2 = new Account(2, customer2, 20);
        Account account3 = new Account(3, customer3, 30);

        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        ArrayList<Account> arrListAccount = new ArrayList<Account>();

        arrListAccount.add(account1);
        arrListAccount.add(account2);
        arrListAccount.add(account3);

        return arrListAccount;
    }
}
